import { Component, OnInit } from '@angular/core';
import { HeroService, Heroe } from '../services/hero.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-heros',
  templateUrl: './heros.component.html',
  styleUrls: ['./heros.component.css']
})
export class HerosComponent implements OnInit {
heros: Heroe[] = [];

  constructor( private heroService: HeroService, private router: Router) { }

  ngOnInit() {
    this.heros = this.heroService.getAll();
  }

  mostrarMas(actual: string) {
    this.router.navigate(['/heros', actual]);
  }

  agregarHeroe() {
    this.router.navigate(['agregarHeroe']);
  }
}
