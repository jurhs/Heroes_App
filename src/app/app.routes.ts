import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { HerosComponent } from './heros/heros.component';
import { AboutComponent } from './about/about.component';
import { MostrarMasComponent } from './heros/heros/mostrar-mas/mostrar-mas.component';
import { BuscadorComponent } from './buscador/buscador.component';
import { AgregarHeroeComponent } from './heros/heros/agregar-heroe/agregar-heroe.component';

const APP_ROUTES: Routes = [
    { path: 'home', component: HomeComponent},
    { path: 'heros', component: HerosComponent},
    { path: 'about', component: AboutComponent},
    { path: 'search/:palabra', component: BuscadorComponent},
    { path: 'heros/:heroe', component: MostrarMasComponent},
    { path: 'agregarHeroe', component: AgregarHeroeComponent},
    { path: '**', pathMatch: 'full', redirectTo: 'home'}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
