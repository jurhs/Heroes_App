import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MostrarMasComponent } from './mostrar-mas.component';

describe('MostrarMasComponent', () => {
  let component: MostrarMasComponent;
  let fixture: ComponentFixture<MostrarMasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MostrarMasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MostrarMasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
