import { Component, OnInit } from '@angular/core';
import { HeroService, Heroe } from '../../../services/hero.service';

@Component({
  selector: 'app-agregar-heroe',
  templateUrl: './agregar-heroe.component.html',
  styleUrls: ['./agregar-heroe.component.css']
})
export class AgregarHeroeComponent implements OnInit {
  heroe: Heroe;

  constructor(private heroService:HeroService) { }

  ngOnInit() {
  }

}
