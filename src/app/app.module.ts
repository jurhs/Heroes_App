import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HeroService } from './services/hero.service';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { HerosComponent } from './heros/heros.component';
import { AboutComponent } from './about/about.component';
import { APP_ROUTING } from './app.routes';
import { MostrarMasComponent } from './heros/heros/mostrar-mas/mostrar-mas.component';
import { BuscadorComponent } from './buscador/buscador.component';
import { AgregarHeroeComponent } from './heros/heros/agregar-heroe/agregar-heroe.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    HerosComponent,
    AboutComponent,
    MostrarMasComponent,
    BuscadorComponent,
    AgregarHeroeComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING,
  ],
  providers: [
    HeroService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
