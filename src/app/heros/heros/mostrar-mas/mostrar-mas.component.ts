import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeroService, Heroe } from '../../../services/hero.service';

@Component({
  selector: 'app-mostrar-mas',
  templateUrl: './mostrar-mas.component.html',
  styleUrls: ['./mostrar-mas.component.css']
})
export class MostrarMasComponent implements OnInit {
  nombre: any;
  heroe: Heroe;
  entrar: boolean;

  constructor(private activatedRoute: ActivatedRoute, private heroService: HeroService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(name => {
      this.nombre = name['heroe'];
    });
    this.heroe = this.heroService.devolverHeroe(this.nombre);
    if(this.heroService.devolverHeroe(this.nombre) != false){
      this.entrar = true;
    }else {
      this.entrar = false;
    }
  }

}
