import { Component, OnInit } from '@angular/core';
import { HeroService, Heroe } from '../services/hero.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.css']
})
export class BuscadorComponent implements OnInit {
  heros: Heroe[] = [];
  nombre: string;
  entrar: boolean;

  constructor( private heroService: HeroService, private router: Router, private activatedRouter: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRouter.params.subscribe(nombre => {
      this.nombre = nombre['palabra']
    })
    this.heros = this.heroService.getHeros(this.nombre);
    if(this.heroService.getHeros(this.nombre) != false){
      this.entrar = true;
    }else {
      this.entrar = false;
    }
    console.log(this.nombre, this.entrar);
  }

  mostrarMas(actual: string) {
    this.router.navigate(['/heros', actual]);
  }
}
